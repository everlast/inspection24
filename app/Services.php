<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'services';
    
    protected $fillable = [
          'mark',
          'model',
          'year',
          'country_number',
          'category_tc',
          'mileage',
          'vin',
          'chassis',
          'body',
          'brake_system',
          'fuel',
          'tire',
          'weight',
          'load',
          'fio_owner',
          'document_type',
          'seria',
          'date',
          'issued_by',
          'city',
          'email',
          'phone'
    ];
    

    public static function boot()
    {
        parent::boot();

        Services::observe(new UserActionsObserver);
    }
    
    
    
    
}