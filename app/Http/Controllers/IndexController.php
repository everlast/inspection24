<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Services;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Mail\CreateService;


class IndexController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        $status = 200;
        try {
            $service = new Services();
            $service->mark = $request->input('mark');
            $service->model = $request->input('mark');
            $service->year = $request->input('year');
            $service->country_number = $request->input('country_number');
            $service->category_tc = $request->input('category_tc');
            $service->mileage = $request->input('mileage');
            $service->vin = $request->input('vin');
            $service->chassis = $request->input('chassis');
            $service->body = $request->input('body');
            $service->brake_system = $request->input('brake_system');
            $service->fuel = $request->input('fuel');
            $service->tire = $request->input('tire');
            $service->weight = $request->input('weight');
            $service->load = $request->input('load');
            $service->fio_owner = $request->input('fio_owner');
            $service->document_type = $request->input('document_type');
            $service->seria = $request->input('seria');
            $service->date = $request->input('date');
            $service->issued_by = $request->input('issued_by');
            $service->city = $request->input('city');
            $service->email = $request->input('email');
            $service->phone = $request->input('phone');
            if (!$service->save()) {
                throw new \Exception('Ошибка при записи в базу');
            }
            $images = $request->file('images');
            if (count($images)) {
                foreach ($images as $image) {
                    $fileName = Str::uuid() . '.' . $image->extension();
                    Storage::putFileAs('public' . DIRECTORY_SEPARATOR, $image, $fileName);
                    $image = new Image();
                    $image->name = $fileName;
                    $image->service_id = $service->id;
                    $image->save();
                }
            }
            Mail::to(env('ADMIN_EMAIL'))->send(new CreateService($service));

            $response = ['status' => 200];
        } catch (\Exception $exception) {
            $response = ['status' => 404, 'message' => $exception->getMessage()];
            $status = 404;
        }
        return response()->json($response, $status);
    }

}
