<?php

namespace App\Mail;

use App\Services;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateService extends Mailable
{
    use Queueable, SerializesModels;

    public $service;


    /**
     * CreateService constructor.
     * @param Services $service
     */
    public function __construct(Services $service)
    {
        $this->service = $service;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.service');
    }
}
