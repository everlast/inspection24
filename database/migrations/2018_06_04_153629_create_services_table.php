<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateServicesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('services',function(Blueprint $table){
            $table->increments("id");
            $table->string("mark")->nullable();
            $table->string("model")->nullable();
            $table->string("year")->nullable();
            $table->string("country_number")->nullable();
            $table->string("category_tc")->nullable();
            $table->string("mileage")->nullable();
            $table->string("vin")->nullable();
            $table->string("chassis")->nullable();
            $table->string("body")->nullable();
            $table->string("brake_system")->nullable();
            $table->string("fuel")->nullable();
            $table->string("tire")->nullable();
            $table->string("weight")->nullable();
            $table->string("load")->nullable();
            $table->string("fio_owner")->nullable();
            $table->string("document_type")->nullable();
            $table->string("seria")->nullable();
            $table->string("date")->nullable();
            $table->string("issued_by")->nullable();
            $table->string("city")->nullable();
            $table->string("email")->nullable();
            $table->string("phone")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }

}