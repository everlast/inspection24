import Swal from 'sweetalert2';

require('semantic-ui-dropdown');

/*
require('jquery-validation');
*/

/*$("#data").calendar({
    type: "date",
    startMode: "year",
    text: {
        days: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Окрябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня",
        now: "Сейчас"
    }
});*/
/*$(".step-1 .from1-to-2").click(function (e) {
    e.preventDefault();
    $(".step-1").removeClass("active");
    $(".step-2-1").addClass("active");
});
$(".step-1 .btn-outline").click(function (e) {
    e.preventDefault();
    $(".step-1").removeClass("active");
    $(".step-2-1").addClass("active")
});*/
$(".second-step-title a.go-to-2-2").click(function (e) {
    e.preventDefault();
    $(".step-2-1").removeClass("active");
    $(".step-2-2").addClass("active");
});
$(".second-step-title a.go-to-2-1").click(function (e) {
    e.preventDefault(), $(".step-2-2").removeClass("active"), $(".step-2-1").addClass("active")
});
/*
$(".second-step-title a.active").click(function (e) {
    e.preventDefault()
});
$(".step-2-1 .btn").click(function (e) {
    e.preventDefault();
    let form = $("#services");

    form.parsley().on('field:validated', function() {
        var ok = $('.parsley-error').length === 0;
        $(".step-2-1").removeClass("active");
        $(".step-3").addClass("active");
    })
        .on('form:submit', function() {
            return false;
        });
});*/
let form = $("#services");
let $sections = $('.f-section');

function navigateTo(index) {
    $sections
        .removeClass('active')
        .eq(index)
        .addClass('active');
    $('.navigation .previous').toggle(index > 0);
    let atTheEnd = index >= $sections.length - 1;
    $('.navigation .next').toggle(!atTheEnd);
    $('.navigation [type=submit]').toggle(atTheEnd);
}

$sections.each(function (index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
});
navigateTo(0);

function curIndex() {
    return $sections.index($sections.filter('.active'));
}

$(document).on('click', '#services .active .navigation .previous', function (e) {
    e.preventDefault();
    navigateTo(curIndex() - 1);
});
$(document).on('click', '#services .to-begin', function (e) {
    e.preventDefault();
    navigateTo(0);
});

$(document).on('click', '#services .active .navigation .next', function (e) {
    e.preventDefault();
    form.parsley().whenValidate({
        group: 'block-' + curIndex()
    }).done(function () {
        navigateTo(curIndex() + 1);
    });
});
form.on('submit', function(e){
    e.preventDefault();
    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    };
    const files = document.querySelector('#file');

    let data = new FormData();
    let formData = $(this).serializeArray();
    for (let item in formData){
        data.append(formData[item].name, formData[item].value);
    }
    for (let i = 0; i < files.files.length; i++) {
        let file = files.files.item(i);
        data.append('images[' + i + ']', file, file.name);
    }

    axios.post('/service/send', data, config)
        .then(function (response) {
            if(response.data.status == 200) {
                $(".step-3").removeClass("active");
                $(".step-4").addClass("active");
                $("#services")[0].reset();
            }
        })
        .catch(function (error) {
            Swal('Ошибка отправки заявки');
        });
});

/*

$(".step-2-1 .back").click(function (e) {
    e.preventDefault();
    $(".step-2-1").removeClass("active");
    $(".step-1").addClass("active");
});
$(".step-2-2 .steps-navigation .btn").click(function (e) {
    e.preventDefault();
    let form = $("#services");
    console.log(form);
    form.validate({
        errorPlacement: function ( error, element ) {
            error.addClass( "ui red pointing label transition" );
            error.insertAfter( element.parent() );
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".row" ).addClass( errorClass );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( ".row" ).removeClass( errorClass );
        }
    });
    if (form.valid() == true) {
        $(".step-2-2").removeClass("active");
        $(".step-3").addClass("active");
    }
});
$(".step-2-2 .back").click(function (e) {
    e.preventDefault();
    $(".step-2-2").removeClass("active");
    $(".step-1").addClass("active");
});
$(".step-3 .btn").click(function (e) {
    e.preventDefault();
    $(".step-3").removeClass("active");
    $(".step-4").addClass("active");
});
$(".step-3 .back").click(function (e) {
    e.preventDefault();
    $(".step-3").removeClass("active");
    $(".step-2-1").addClass("active");
});

$(".step-4 .btn").click(function (e) {
    e.preventDefault();
    $(".step-4").removeClass("active");
    $(".step-1").addClass("active");
});
*/


$(document).ready(function () {
    $(".accordion-item-hat").click(function (o) {
        o.preventDefault(), $(".accordion-item-hat").each(function () {
            $(this).parent().removeClass("open")
        }), $(this).parent().toggleClass("open")
    });
    $(".ui.dropdown").dropdown();
    $(".call-order-btn").click(function (o) {
        o.preventDefault(), $(".call-order-modal").modal({
            inverted: !0,
            transition: "scale",
            autofocus: !1
        }).modal("show")
    }), $(".menu-button").click(function (o) {
        o.preventDefault(), $(".menu-modal").modal({inverted: !0, transition: "scale", autofocus: !1}).modal("show")
    }), $(".check").click(function (o) {
        o.preventDefault(), $(".success").modal({inverted: !0, transition: "scale", autofocus: !1}).modal("show")
    }), $(".check-false").click(function (o) {
        o.preventDefault(), $(".error").modal({inverted: !0, transition: "scale", autofocus: !1}).modal("show")
    })
}), $(window).on("load", function () {
    var o = $("#page-preloader");
    o.find(".spinner").fadeOut(), o.delay(350).fadeOut("slow")
});


