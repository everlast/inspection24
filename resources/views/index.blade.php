<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">

    <meta name="description" content="This is description">
    <meta name="keywords" content="keywords">
    <title>Техосмотр24 online</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">

    <style>
        #page-preloader {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            background: #fff;
            z-index: 100500;
        }
        #page-preloader .spinner {
            width: 139px;
            height: 66px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin: -33px 0 0 -70px;
            background: url('images/logo.png') no-repeat 50% 50%;
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
        }
        @-webkit-keyframes preloader-anim {
            0% {
                opacity: 0;
                -webkit-transform: translateY(100px);
                transform: translateY(100px);
            }

            100% {
                opacity: 1;
                -webkit-transform: translateY(0);
                transform: translateY(0);
            }
        }
        @keyframes preloader-anim {
            0% {
                opacity: 0;
                -webkit-transform: translateY(100px);
                -ms-transform: translateY(100px);
                transform: translateY(100px);
            }

            100% {
                opacity: 1;
                -webkit-transform: translateY(0);
                -ms-transform: translateY(0);
                transform: translateY(0);
            }
        }
        .preloader-anim {
            -webkit-animation-name: preloader-anim;
            animation-name: preloader-anim;
        }
    </style>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/app.css">

    <!--[if lt IE 9]>v
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- START Preloader -->
<div id="page-preloader"><span class="spinner preloader-anim"></span></div>
<!-- Finish Preloader -->

<header class="header">
    <div class="container">
        <div class="header-flex">
            <a href="#" class="header-logo">
                <img src="images/logo.png" alt="logo">
            </a>
            <ul class="hide-tablet">
                <li>
                    <a href="#">Как работает</a>
                </li>
                <li>
                    <a href="#">Услуги</a>
                </li>
                <li>
                    <a href="#">Преимущества</a>
                </li>
                <li>
                    <a href="#">Вопросы</a>
                </li>
                <li>
                    <a href="#">Партнеры</a>
                </li>
                <li>
                    <a href="#">Оплата</a>
                </li>
            </ul>
            <div class="header-phone-block hide-mobile">
                <a href="tel:0088005051570" class="header-phone">
                    <i class="fas fa-phone"></i>
                    8 (800) 505-15-70
                </a>
                <a href="#" class="contacts-link">смотреть все контакты</a>
            </div>
            <a href="#" class="call-order-btn hide-mobile">
                <i class="fas fa-phone"></i>
            </a>
            <a href="#" class="menu-button visible-tablet"><i class="fas fa-bars"></i> Menu</a>
        </div>
    </div>
</header>

<div class="ui modal menu-modal">
    <i class="far fa-times-circle close"></i>
    <div class="modal-content">
        <div class="menu-modal-header">
            <div class="header-phone-block">
                <a href="tel:0088005051570" class="header-phone">
                    <i class="fas fa-phone"></i>
                    8 (800) 505-15-70
                </a>
                <a href="#" class="contacts-link">смотреть все контакты</a>
            </div>
            <a href="#" class="call-order-btn">
                <i class="fas fa-phone"></i>
            </a>
        </div>
        <ul>
            <li>
                <a href="#">Как работает</a>
            </li>
            <li>
                <a href="#">Услуги</a>
            </li>
            <li>
                <a href="#">Преимущества</a>
            </li>
            <li>
                <a href="#">Вопросы</a>
            </li>
            <li>
                <a href="#">Партнеры</a>
            </li>
            <li>
                <a href="#">Оплата</a>
            </li>
        </ul>
    </div>
</div>

<div class="ui modal call-order-modal">
    <i class="far fa-times-circle close"></i>
    <form action="#">
        <div class="modal-content">
            <p class="modal-content-title">
                Обратный звонок
            </p>
            <label>
                Как вас зовут?<br>
                <input type="text" placeholder="Иван Петрович">
            </label>
            <br>
            <label>
                Ваш номер телефона<br>
                <input type="tel" placeholder="+7 - ХХХ-ХХХ-ХХХ">
            </label>
            <br>
            <label>
                Удобное время звонка<br>
                <input type="text" placeholder="с 14 до 18 часов">
            </label>
            <br>
            <label>
                <input type="checkbox"> Согласен на обработку персональных данных
            </label>
        </div>
        <div class="actions">
            <button type="submit" class="btn approve">Отправить заявку</button>
        </div>
    </form>
</div>

<main>
    <section class="cover dark-section">
        <div class="container">
            <h1>Техосмотр для ОСАГО <br><span>с онлайн проверкой</span></h1>
            <div class="cover-list">
                <div class="line"></div>
                <p>Заполни заявку на получение диагностической карты</p>
                <ul>
                    <li><span>Без предоплаты</span></li>
                    <li><span>Быстрое оформление - 10 минут</span></li>
                    <li><span>Аккредитованные СТО с внесением в базу ГИБДД</span></li>
                    <li><span>Самая низкая цена - 550 рублей</span></li>
                </ul>
            </div>
            <a href="#" class="btn">Пройти техосмотр cейчас</a>
            <img src="images/cover/laptop.png" alt="img">
        </div>
    </section>
    <section class="steps-description">
        <div class="container">
            <div class="steps-description-flex">
                <div class="step-descr">
                    <img src="images/steps-descr/icon-1.png" alt="icon">
                    <p class="step-descr-title">Шаг 1</p>
                    <div class="line"></div>
                    <p>Заполяете заявку на сайте или присылаете нам фотографию ваших документов</p>
                </div>
                <div class="step-descr">
                    <img src="images/steps-descr/icon-2.png" alt="icon">
                    <p class="step-descr-title">Шаг 2</p>
                    <div class="line"></div>
                    <p>Наши специалисты обрабатывают вашу заявку. После обработки мы уведомим вас об этом</p>
                </div>
                <div class="step-descr">
                    <img src="images/steps-descr/icon-3.png" alt="icon">
                    <p class="step-descr-title">Шаг 3</p>
                    <div class="line"></div>
                    <p>Ручная проверка карты в базе ГИБДД ЕАИСТО</p>
                </div>
                <div class="step-descr">
                    <img src="images/steps-descr/icon-4.png" alt="icon">
                    <p class="step-descr-title">Шаг 4</p>
                    <div class="line"></div>
                    <p>Оплата услуги удобным для вас способом: банковской картой, электронными деньгами</p>
                </div>
            </div>
        </div>
    </section>
    <section class="steps">
        <div class="container">
            <div class="white-block">
                <div>
                    <h3>
                        <img src="images/h3.png" alt="img">
                        <br>
                        Наши услуги
                    </h3>
                    <div class="steps-title">
                        Оставить заявку на прохождение ТО
                    </div>
                    <div class="steps-items">
                        <form action="{{route('service.send')}}" method="POST" id="services"
                              enctype="multipart/form-data"
                              data-parsley-validate="">
                            <div class="step active step-1 f-section">
                                <div class="step-title">Введите номер автомобиля</div>
                                <div class="steps-ticks">
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                </div>
                                <div class="step-content navigation">
                                    <input type="text" placeholder="А1234БВ">
                                    <a href="#" class="next btn from1-to-2">Далее</a>
                                    <span>или</span>
                                    <a href="#" class="next btn btn-outline">Ввести вручную</a>
                                </div>
                            </div>
                            <div class="step step-2-1 f-section">
                                <div class="step-title">Основная информация</div>
                                <div class="steps-ticks">
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                </div>
                                <div class="step-content">
                                    <div class="second-step-title">
                                        <a href="#" class="active">Заполнить форму</a>
                                        <span>/</span>
                                        <a href="#"  class="go-to-2-2">Загрузить документы</a>
                                    </div>
                                    <div class="second-step-form-flex">
                                        <label>
                                            <span>Марка ТС</span><br>
                                            <select class="ui search dropdown" name="mark" required data-parsley-errors-container="#mark">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="mark"></span>
                                        </label>
                                        <label>
                                            <span>Модель ТС</span><br>
                                            <select class="ui search dropdown" name="model" required data-parsley-errors-container="#model">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="model"></span>
                                        </label>
                                        <label>
                                            <span>Год выпуска</span><br>
                                            <select class="ui search dropdown" name="year" required data-parsley-errors-container="#year">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="year"></span>
                                        </label>
                                        <label>
                                            <span>Гос. номер</span><br>
                                            <select class="ui search dropdown" name="country_number" required  data-parsley-errors-container="#country_number">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="country_number"></span>
                                        </label>
                                        <label>
                                            <span>Категория ТС (ОКП)</span><br>
                                            <select class="ui search dropdown" name="category_tc" required data-parsley-errors-container="#category_tc">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="category_tc"></span>
                                        </label>
                                        <label>
                                            <span>Пробег (км)</span><br>
                                            <input type="text" placeholder="1234" name="mileage" required>
                                        </label>
                                        <label>
                                            <span>VIN код</span><br>
                                            <input type="text" placeholder="xxxxxxxxxxxx" name="vin" required>
                                        </label>
                                        <label>
                                            <span>№ шасси (рамы)</span><br>
                                            <input type="text" placeholder="если есть" required name="chassis">
                                        </label>
                                        <label>
                                            <span>№ кузова</span><br>
                                            <input type="text" placeholder="если есть" name="body" required>
                                        </label>
                                        <label>
                                            <span>Тормозная система</span><br>
                                            <select class="ui search dropdown" name="brake_system" required data-parsley-errors-container="#brake_system">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="brake_system"></span>
                                        </label>
                                        <label>
                                            <span>Топливо</span><br>
                                            <select class="ui search dropdown" name="fuel" required data-parsley-errors-container="#fuel">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="fuel"></span>
                                        </label>
                                        <label>
                                            <span>Марка шин</span><br>
                                            <select class="ui search dropdown" name="tire" required data-parsley-errors-container="#tire">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="tire"></span>
                                        </label>
                                        <label>
                                            <span>Разрешенная масса (кг)</span><br>
                                            <input type="text" placeholder="1234" name="weight" required>
                                        </label>
                                        <label>
                                            <span>Масса без нагрузки (кг)</span><br>
                                            <input type="text" placeholder="1234" name="load" required>
                                        </label>
                                        <label>
                                            <span>ФИО собственника ТС</span><br>
                                            <input type="text" placeholder="Иванов Иван Иванович" name="fio_owner" required>
                                        </label>
                                        <label>
                                            <span>Тип документа</span><br>
                                            <select class="ui search dropdown" name="document_type"
                                                    required data-parsley-errors-container="#document_type">
                                                <option value="">Выбрать</option>
                                                <option value="AZ">Марка 1</option>
                                                <option value="AZ">Марка 2</option>
                                                <option value="AZ">Марка 3</option>
                                                <option value="AZ">Марка 4</option>
                                                <option value="AZ">Марка 5</option>
                                                <option value="AZ">Марка 6</option>
                                                <option value="AZ">Марка 7</option>
                                                <option value="AZ">Марка 8</option>
                                                <option value="AZ">Марка 9</option>
                                            </select>
                                            <span class="parsley-errors-list" id="document_type"></span>
                                        </label>
                                        <label>
                                            <span>Серия и номер документа</span><br>
                                            <input type="text" placeholder="34АВ 123456" name="seria" required>
                                        </label>
                                        <label>
                                            <span>Дата выдачи</span><br>
                                            <div class="ui calendar" id="data">
                                                <div class="ui input left icon">
                                                    <i class="calendar icon"></i>
                                                    <input type="text" placeholder="ммм дд, гггг" name="date" required>
                                                </div>
                                            </div>
                                        </label>
                                        <label class="long-label">
                                            <span>Кем выдан</span><br>
                                            <input type="text" placeholder="МО ГИБДД №1 ГУ МВД России по Москве" name="issued_by" required>
                                        </label>
                                    </div>
                                </div>
                                <div class="steps-navigation navigation">
                                    <a href="#" class="back previous"><i class="fas fa-chevron-left"></i> Вернуться</a>
                                    <a href="#" class="btn next">Далее</a>
                                </div>
                            </div>
                            <div class="step step-2-2 f-section">
                                <div class="step-title">Основная информация</div>
                                <div class="steps-ticks">
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                </div>
                                <div class="step-content">
                                    <div class="second-step-title">
                                        <a href="#" class="go-to-2-1">Заполнить форму</a>
                                        <span>/</span>
                                        <a href="#"  class="active">Загрузить документы</a>
                                    </div>
                                    <div class="second-step-files-flex">
                                        <div class="second-step-file">
                                            <img src="images/steps/letter-1.jpg" alt="img">
                                            <p>Свидетельство о регистрации ТС</p>
                                        </div>
                                        <div class="second-step-file">
                                            <img src="images/steps/letter-2.jpg" alt="img">
                                            <p>Паспорт ТС</p>
                                        </div>
                                        <div class="second-step-files-content">
                                            <p><span>Для тех, кто не хочет тратить время на заполнение формы:</span> выберите нужный тип документа и загрузите его скан-копию или фотографию.</p>
                                            <p class="files-terms">Файл должен быть в хорошем качестве; <br>формата: jpg. png; размером/объемом до 3Мб</p>
                                            <div class="choose-file-btn">
                                                <input type="file" id="file" style="display: none" name="documents" multiple>
                                                <label for="file" class="btn btn-outline">
                                                    Выберите файл
                                                </label>
                                                <span>Файл не выбран</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="step-contacts">
                                        <div class="step-contacts-line">
                                            <div>или</div>
                                        </div>
                                        <p>Отправьте документ удобным для вас способом</p>
                                        <div class="step-contacts-flex">
                                            <a href="tel:0089270602676"><i class="fab fa-whatsapp"></i> 8 927 060 2676</a>
                                            <a href="tel:0089270602676"><i class="fab fa-telegram"></i> 8 927 060 2676</a>
                                            <a href="tel:0089270602676"><i class="fab fa-viber"></i> 8 927 060 2676</a>
                                            <a href="mailto:mail@yandex.ru"><i class="fas fa-envelope"></i> mail@yandex.ru</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="steps-navigation navigation">
                                    <a href="#" class="back previous"><i class="fas fa-chevron-left"></i> Вернуться</a>
                                    <a href="#" class="btn next">Далее</a>
                                </div>
                            </div>
                            <div class="step step-3 f-section">
                                <div class="step-title">Основная информация</div>
                                <div class="steps-ticks">
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick ">
                                        <i class="fas fa-check"></i>
                                    </div>
                                </div>
                                <div class="step-content">
                                    <div class="third-step-flex">
                                        <label>
                                            <span>Город</span><br>
                                            <input type="text" placeholder="Москва" name="city" required>
                                        </label>
                                        <label>
                                            <span>Email адрес</span><br>
                                            <input type="email" placeholder="mail@yandex.ru" name="email" required>
                                        </label>
                                        <label>
                                            <span>Номер телефона</span><br>
                                            <input type="tel" placeholder="+7-ххх-ххххххх" name="phone" required>
                                        </label>
                                    </div>
                                    <label>
                                        <input type="checkbox" name="personal" required data-parsley-errors-container="#personal"> Согласен на обработку персональных данных
                                        <span class="parsley-errors-list" id="personal"></span>
                                    </label>
                                </div>
                                <div class="steps-navigation navigation">
                                    <a href="#" class="previous back"><i class="fas fa-chevron-left"></i> Вернуться</a>
                                    <button type="submit" class="btn" id="submit_form">Оформить заявку</button>
                                </div>
                            </div>
                            <div class="step step-4">
                                <div class="step-title">Завершение</div>
                                <div class="steps-ticks">
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="tick active">
                                        <i class="fas fa-check"></i>
                                    </div>
                                </div>
                                <div class="step-content last-step">
                                    <p>Поздравляем, ваша заявка успешно принята.</p>
                                    <p>Ожидайте, наши специалисты свяжутся с вами в ближайшее время</p>
                                </div>
                                <div class="steps-navigation">
                                    <a href="#" class="btn btn-outline to-begin">Подать еще заявку</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="advantages">
        <div class="container">
            <h3>
                <img src="images/h3.png" alt="img">
                <br>
                Наши преимущества
            </h3>
            <div class="advantages-flex">
                <div class="advantages-item">
                    <img src="images/advantages/icon1.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Официально</p>
                        <div class="line"></div>
                        <p>Мгновенное внесение в базу ЕАИСТО ГИБДД</p>
                    </div>
                </div>
                <div class="advantages-item">
                    <img src="images/advantages/icon2.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Надежно</p>
                        <div class="line"></div>
                        <p>Работаем с 2016 <br>Оформлено >2000 карт</p>
                    </div>
                </div>
                <div class="advantages-item">
                    <img src="images/advantages/icon3.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Без посредников</p>
                        <div class="line"></div>
                        <p>Операторы ТО аккредитованы РСА</p>
                    </div>
                </div>
                <div class="advantages-item">
                    <img src="images/advantages/icon4.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Удобно</p>
                        <div class="line"></div>
                        <p>Платите после проверки карты по базе ЕАИСТО ГИБДД</p>
                    </div>
                </div>
                <div class="advantages-item">
                    <img src="images/advantages/icon5.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Все категории авто</p>
                        <div class="line"></div>
                        <p>Единая стоимость для всех видов транспорта</p>
                    </div>
                </div>
                <div class="advantages-item">
                    <img src="images/advantages/icon6.png" alt="icon">
                    <div>
                        <p class="advantages-item-title">Онлайн поддержка</p>
                        <div class="line"></div>
                        <p>Оперативная поддрежка в рабочие дни с 8:30 до 23:30</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
        <div class="faq-gray-line"></div>
        <div class="container">
            <h3>
                <img src="images/h3.png" alt="img">
                <br>
                Частые вопросы
            </h3>
            <div class="faq-flex">
                <div class="faq-accordion">
                    <div class="accordion-item open">
                        <div class="accordion-item-hat">
                            Что такое диагностическая карта и зачем она нужна? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Какие есть способы оплаты? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Что будет с диагностической картой, если ее не оплатить? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Какая дата оформления будет в диагностической карте? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Что будет с заявкой, если при вводе информации были допущены ошибки? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Страховые компании принимают диагностические карты? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-item-hat">
                            Диагностическая карта официальная? <img src="images/faq/arrow-down.png" alt="arrow"><img src="images/faq/arrow-up.png" alt="arrow">
                        </div>
                        <div class="accordion-item-content">
                            Диагностическая карта является легитимной. Она вносится в официальную базу ГИБДД ЕАИСТО. Для проверки данных Вашей карты после её получения, <a href="#">перейдите по ссылке</a>.
                        </div>
                    </div>
                </div>
                <div class="faq-form-container">
                    <p class="faq-form-title">
                        Остались вопросы?
                    </p>
                    <form action="#">
                        <label>
                            <span>Как вас зовут?</span><br>
                            <input type="text" placeholder="Иван Петрович">
                        </label>
                        <br>
                        <label>
                            <span>Ваш номер телефона</span><br>
                            <input type="tel" placeholder="+7 - ХХХ-ХХХ-ХХХ">
                        </label>
                        <br>
                        <label>
                            <span>Ваш email адрес</span><br>
                            <input type="email" placeholder="mail@yandex.ru">
                        </label>
                        <br>
                        <label class="checkbox-agree">
                            <input type="checkbox"> Согласен на обработку персональных данных
                        </label>
                        <br>
                        <button type="submit" class="btn">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="faq-gray-line"></div>
    </section>
    <section class="partners">
        <div class="container">
            <h3>
                <img src="images/h3.png" alt="img">
                <br>
                Наши партнеры
            </h3>
            <div class="partners-flex">
                <a href="#">
                    <img src="images/partners/logo-1.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-2.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-3.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-4.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-5.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-6.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-7.png" alt="logo">
                </a>
                <a href="#">
                    <img src="images/partners/logo-8.png" alt="logo">
                </a>
            </div>
        </div>
    </section>
    <section class="baza">
        <div class="container">
            <div class="white-block">
                <div>
                    <div class="baza-title">
                        Проверить в базе ГБДДД
                    </div>
                    <p>Введите номер диагностической карты</p>
                    <form action="#">
                        <input type="text" placeholder="ххххххххххххххх"><button type="submit" class="btn check">Проверить</button><!-- <button class="check-false">Временная кнопка для теста ошибки</button> -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="payment">
        <div class="container">
            <h3>
                <img src="images/h3.png" alt="img">
                <br>
                Способы оплаты
            </h3>

            <form method="POST" action="https://money.yandex.ru/eshop.xml">
                <input type="hidden" name="shopid" value="'.$settings['yandex_shopid'].'">
                <input type="hidden" name="sum" value="'.$price.'">
                <input type="hidden" name="scid" value="'.$settings['yandex_scid'].'">

                {{--<input type="hidden" name="shopSuccessURL" value="'.$success_url.'">
                <input type="hidden" name="shopFailURL" value="'.$fail_url.'">
                --}}
                {{--<input type="hidden" name="cps_email" value="'.htmlspecialchars($order->email,ENT_QUOTES).'">
                <input type="hidden" name="cps_phone" value="'.htmlspecialchars($order->phone,ENT_QUOTES).'">
                --}}
                {{--<input type="hidden" name="paymenttype" value="'.$settings['yandex_paymenttype'].'">--}}
                <input type="hidden" name="customerNumber" value="'.$order->id.'">
                <input type="submit" name="submit-button" value="'.$button_text.'" class="checkout_button">
            </form>

            <form action="#">
                <div>
                    <label class="payment-flex">
                        <div class="payment-flex-left">Назначение перевода</div>
                        <input type="text" placeholder="Номер диагностической карты">
                    </label>
                    <label class="payment-flex">
                        <div class="payment-flex-left">Сумма</div>
                        <input type="text" placeholder="650"> <span class="hide-mobile">руб.</span>
                    </label>
                    <div class="payment-flex">
                        <div class="payment-flex-left">Способ оплаты</div>
                        <label>
                            <input type="radio" name="payment">
                            <div class="payment-type">
                                <img src="images/payment/yandex.png" alt="logo">
                            </div>
                        </label>
                        <label>
                            <input type="radio" name="payment">
                            <div class="payment-type">
                                <img src="images/payment/visa.png" alt="logo">
                            </div>
                        </label>
                        <label>
                            <input type="radio" name="payment">
                            <div class="payment-type">
                                <img src="images/payment/master-card.png" alt="logo">
                            </div>
                        </label>
                    </div>
                </div>
                <button class="btn">Перевести</button>
            </form>
        </div>
    </section>
    <section class="contacts dark-section">
        <div class="container">
            <div class="white-block">
                <h3>
                    <img src="images/h3.png" alt="img">
                    <br>
                    Контакты и реквизиты
                </h3>
                <div class="contacts-flex">
                    <div class="contacts-item">
                        <img src="images/contacts/icon-1.png" alt="icon">
                        <div>
                            <p class="contacts-item-title">Телефон</p>
                            <div class="line"></div>
                            <a href="tel:88005051570">8 (800) 505-15-70</a>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <img src="images/contacts/icon-2.png" alt="icon">
                        <div>
                            <p class="contacts-item-title">Email</p>
                            <div class="line"></div>
                            <a href="mailto:support@е-техосмотр.рф">support@е-техосмотр.рф</a>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <img src="images/contacts/icon-3.png" alt="icon">
                        <div>
                            <p class="contacts-item-title">Время работы</p>
                            <div class="line"></div>
                            <p>c 8:00 до 23:00 (МСК),<br>без выходных</p>
                        </div>
                    </div>
                </div>
                <p>Общество с ограниченной ответственностью «АвтоКонтроль» (ООО «АвтоКонтроль») — руководитель Василец Александр Николаевич. Зарегистрирован в реестре № 1804 от 11.01.2012 по адресу 143040, Московская обл., Одинцовский р-н, г. Голицыно, проезд Промышленный, 5</p>
            </div>
            <p class="contacts-footer">Техосмотр недорого - сервис приема онлайн заявок на техосмотр, для последующего прохождения в авторизованном центре. Напоминаем: без техосмотра нельзя купить ОСАГО. Если Вы решили купить ОСАГО не забудьте о покупке диагностической карты - сделайте правильный выбор и обратитесь к профессионалам! Формируя сообщения и онлайн заявки на сайте, Вы соглашаетесь на обработку ваших персональных данных, приём звонков, получение сообщений, а также на информационную рассылку.</p>
        </div>
    </section>
</main>

<div class="ui modal success baza-modal">
    <i class="far fa-times-circle close"></i>
    <form action="#">
        <div class="modal-content">
            <div class="baza-modal-title">
                Карта 087000011803148 найдена в базе ЕАИСТО
            </div>
            <p><span>Дата выдачи:</span> 21.03.2018</p>
            <p><span>Срок действия:</span> 21.03.2018</p>
            <p><span>VIN:</span> JSAJTDA4V00213347</p>
            <p><span>Марка:</span> Suzuki</p>
            <p><span>Модель:</span> Grand Vitara</p>
            <p><span>Рег. номер:</span> 0300CX36</p>
            <p><span>Эксперт:</span> Котельников</p>
            <p><span>Оператор:</span> 08700 ООО “ТО МАКС-ЧЕРНОЗЕМЬЕ”</p>
        </div>
        <div class="actions">
            <div class="btn btn-outline approve">Закрыть</div>
        </div>
    </form>
</div>

<div class="ui modal error baza-modal">
    <i class="far fa-times-circle close"></i>
    <form action="#">
        <div class="modal-content">
            <div class="baza-modal-title">
                Карта не найдена в базе ЕАИСТО
            </div>
        </div>
        <div class="actions">
            <div class="btn btn-outline approve">Закрыть</div>
        </div>
    </form>
</div>

<footer class="footer">
    <div class="container">
        <div class="footer-flex">
            <p>Сайт не является офертой, определяемой <br class="hide-tablet">положениями ст. 437 ГК РФ</p>
            <img src="images/footer/logo-1.png" alt="logo">
            <img src="images/footer/logo-2.png" alt="logo">
            <img src="images/footer/logo-3.png" alt="logo">
            <img src="images/footer/logo-4.png" alt="logo">
            <img src="images/footer/logo-5.png" alt="logo">
            <img src="images/footer/logo-6.png" alt="logo">
            <p>© 2012-2017, Компания ПТО.рф <br class="hide-tablet">Пункт Технического Осмотра</p>
        </div>
    </div>
</footer>

<!-- Scripts -->

<!-- Libs -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script defer src="js/libs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js"></script>
<script defer src="js/app.js"></script>

</body>
</html>
