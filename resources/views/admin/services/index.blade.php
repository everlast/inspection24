@extends('admin.layouts.master')

@section('content')

{{--
<p>{!! link_to_route(config('quickadmin.route').'.services.create', trans('quickadmin::templates.templates-view_index-add_new') , null, array('class' => 'btn btn-success')) !!}</p>
--}}

@if ($services->count())
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">{{ trans('quickadmin::templates.templates-view_index-list') }}</div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                <thead>
                    <tr>
                        <th>
                            {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                        </th>
                        <th>Город</th>
                        <th>Email</th>
                        <th>Номер телефона</th>
                        <th>Марка ТС</th>
<th>Модель ТС</th>
<th>Год выпуска</th>
<th>Гос. номер</th>
{{--<th>Категория ТС (ОКП)</th>
<th>Пробег (км)</th>
<th>VIN код</th>
<th>№ шасси (рамы)</th>
<th>№ кузова</th>
<th>Тормозная система</th>
<th>Топливо</th>
<th>Марка шин</th>
<th>Разрешенная масса (кг)</th>
<th>Масса без нагрузки (кг)</th>
<th>ФИО собственника ТС</th>
<th>Тип документа</th>
<th>Серия и номер документа</th>
<th>Дата выдачи</th>
<th>Кем выдан</th>--}}

                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($services as $row)
                        <tr>
                            <td>
                                {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                            </td>
                            <td>{{ $row->city }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ $row->mark }}</td>
<td>{{ $row->model }}</td>
<td>{{ $row->year }}</td>
<td>{{ $row->country_number }}</td>
{{--<td>{{ $row->category_tc }}</td>
<td>{{ $row->mileage }}</td>
<td>{{ $row->vin }}</td>
<td>{{ $row->chassis }}</td>
<td>{{ $row->body }}</td>
<td>{{ $row->brake_system }}</td>
<td>{{ $row->fuel }}</td>
<td>{{ $row->tire }}</td>
<td>{{ $row->weight }}</td>
<td>{{ $row->load }}</td>
<td>{{ $row->fio_owner }}</td>
<td>{{ $row->document_type }}</td>
<td>{{ $row->seria }}</td>
<td>{{ $row->date }}</td>
<td>{{ $row->issued_by }}</td>--}}

                            <td>
                                {!! link_to_route(config('quickadmin.route').'.services.edit', trans('quickadmin::templates.templates-view_index-edit'), array($row->id), array('class' => 'btn btn-xs btn-info')) !!}
                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.services.destroy', $row->id))) !!}
                                {!! Form::submit(trans('quickadmin::templates.templates-view_index-delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-danger" id="delete">
                        {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                    </button>
                </div>
            </div>
            {!! Form::open(['route' => config('quickadmin.route').'.services.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                <input type="hidden" id="send" name="toDelete">
            {!! Form::close() !!}
        </div>
	</div>
@else
    {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif

@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });
    </script>
@stop