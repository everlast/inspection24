<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('service/send', 'IndexController@send')->name('service.send');
Route::get('/test', function () {
    $data = [
        'timeout' => 30
    ];
    $gibdd = new \bckr75\Gibdd($data);
    $captcha = $gibdd->getCaptchaValue([
        'setCookie' => true,
        'base64' => true
    ]);
    $history = $gibdd->tryGetHistory('JHMCL76408C206307', $captcha);
    dd($history);

    return view('index');
});